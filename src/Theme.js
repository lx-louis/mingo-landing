// src/theme.js
import { extendTheme } from '@chakra-ui/react';

const colors = {
  primary: '#1782E7',
  secondary: '#FFBF44',
  black: '#000000',
  white: '#FFFFFF',
};
const fonts =  {
    heading: 'Alumni Sans, sans-serif',
    body: 'Inter, sans-serif',
};

const styles = {
  global: () => ({
    body: {
      bg: "",
    },
  }),
};

const theme = extendTheme({ colors, fonts, styles });

export default theme;
