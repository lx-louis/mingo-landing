import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './pages/Home/Home';
import Blog from './pages/Blog/Blog';
import Airdrop from './pages/Airdrop/Airdrop';
import Product from './pages/Product/Product';

function App() {
  return (
    <Router>
      <div className='main'>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/blog" element={<Blog />} />
          <Route path="/airdrop" element={<Airdrop />} />
          <Route path="/product" element={<Product />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
