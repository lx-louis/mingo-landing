import './Home.scss';
import homeData from '../../content/home.json';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

import twitterIcon from '../../assets/twitter.svg';
import youtubeIcon from '../../assets/youtube.svg';
import discordIcon from '../../assets/discord.svg';

function Home() {

  const [imageRL2, setImageRL2] = useState('');
  const [imageRL4B1, setImageRL4B1] = useState('');
  const [imageRL4B2, setImageRL4B2] = useState('');
  const [imageRL4B3, setImageRL4B3] = useState('');
  const [imageRL4B4, setImageRL4B4] = useState('');

  useEffect(() => {
    setImageRL2(homeData.route_link_2);
    setImageRL4B1(homeData.blog_img_1);
    setImageRL4B2(homeData.blog_img_2);
    setImageRL4B3(homeData.blog_img_3);
    setImageRL4B4(homeData.blog_img_4);
  }, []);

  return (
    <div className='home_container'>
      <div className='section_1'>
        <h1>{homeData.title_1}</h1>
        <p>{homeData.content_1}</p>
        <div className='link'>
          <Link to='/' className='text'>CTA</Link>
        </div>
      </div>
      <div className='section_2'>
        <div className='feature_content'>
          <h2>{homeData.title_2}</h2>
          <p>{homeData.content_2}</p>
          <div className='link'>
            <Link to='/' className='text'>BUTTON</Link>
          </div>
        </div>
        <img src={imageRL2} />
        
        
      </div>
      <div className='section_3'>
        <div className='pricing_tag'>
          <p className='tag_txt'>{homeData.tag_3}</p>
        </div>
        <h2>{homeData.title_3}</h2>
        <p className='pricing_txt'>{homeData.content_3}</p>
        <div className='cards'>
          <div className='card'>
            <h3 className='card_title'>{homeData.product_title_1}</h3>
            <p className='card_content'>{homeData.product_content_1}</p>
            <Link to={`${homeData.product_route_1}`} className='card_btn'>BUTTON</Link>
          </div>
          <div className='card'>
            <h3 className='card_title'>{homeData.product_title_2}</h3>
            <p className='card_content'>{homeData.product_content_2}</p>
            <Link to={`${homeData.product_route_2}`} className='card_btn'>BUTTON</Link>
          </div>
          <div className='card'>
            <h3 className='card_title'>{homeData.product_title_3}</h3>
            <p className='card_content'>{homeData.product_content_3}</p>
            <Link to={`${homeData.product_route_3}`} className='card_btn'>BUTTON</Link>
          </div>
        </div>
        
      </div>

      <div className='section_4'>
        <div className='blog_tag'>
          <p className='tag_txt'>{homeData.tag_4}</p>
        </div>
        <h2>{homeData.title_4}</h2>
        <p className='blog_txt'>{homeData.content_4}</p>
        <div className='cards'>

          <div className='card'>
            <Link to={`${homeData.blog_route_1}`} className='card_link'>
              <img src={imageRL4B1}/>
              <h3 className='card_title'>{homeData.blog_title_1}</h3>
              <p className='card_content'>{homeData.blog_content_1}</p>
            </Link>
          </div>

          <div className='card'>
            <Link to={`${homeData.blog_route_2}`} className='card_link'>
              <img src={imageRL4B2}/>
              <h3 className='card_title'>{homeData.blog_title_2}</h3>
              <p className='card_content'>{homeData.blog_content_2}</p>
            </Link>
          </div>

          <div className='card'>
            <Link to={`${homeData.blog_route_3}`} className='card_link'>
              <img src={imageRL4B3}/>
              <h3 className='card_title'>{homeData.blog_title_3}</h3>
              <p className='card_content'>{homeData.blog_content_3}</p>
            </Link>
          </div>

          <div className='card'>
            <Link to={`${homeData.blog_route_4}`} className='card_link'>
              <img src={imageRL4B4}/>
              <h3 className='card_title'>{homeData.blog_title_4}</h3>
              <p className='card_content'>{homeData.blog_content_4}</p>
            </Link>
          </div>

        </div>
      </div>
      <div className="section_5">
        <img src={homeData.icon_twi} className='img-logo'/>
        <div className='network_list'>
          <h2>{homeData.title_5}</h2>
          <div className='links'>
            <a href={homeData.twitter} target="_blank" className='network' rel="noreferrer">
              <img src={twitterIcon} alt="Twitter"/>
            </a>
            <a href={homeData.youtube} target="_blank" className='network' rel="noreferrer">
              <img src={youtubeIcon} alt="YouTube"/>
            </a>
            <a href={homeData.discord} target="_blank" className='network' rel="noreferrer">
              <img src={discordIcon} alt="Discord"/>
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
  
export default Home;