import { Link } from 'react-router-dom';
import './Navbar.scss';

function Navbar() {
  return (
    <nav className='navbar'>
      <div className='navbar_left'>
        <h2 className='text'>MINGO AIRDROP</h2>
      </div>
      <div className='navbar_middle'>
        <Link to="/blog">BLOG</Link>
        <Link to="/product">AIRDROP</Link>
        <Link to="/product">PRICING</Link>
      </div>
      <div className='navbar_right'>
        <Link to="/" className='text'>COMMUNITY</Link>
      </div>
    </nav>
  );
}

export default Navbar;
